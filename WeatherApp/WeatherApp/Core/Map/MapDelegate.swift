//
//  MapDelegate.swift
//  WeatherApp
//
//  Created by AxxiomeHealth on 14/12/2022.
//

import Foundation
import MapKit


class MapCustomDelegate: NSObject, MKMapViewDelegate {
    var parent: MKMapView
    
    init(_ parent: MKMapView) {
        self.parent = parent
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
      if let tileOverlay = overlay as? MKTileOverlay {
        let renderer = MKTileOverlayRenderer(overlay: tileOverlay)
        return renderer
      }
      return MKOverlayRenderer()
    }
}

