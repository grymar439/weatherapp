//
//  MapView.swift
//  WeatherApp
//
//  Created by Martyna on 08/11/2022.
//

import SwiftUI
import MapKit

struct HeatmapView: View {
    
    //let directions: MKDirections.Response
    
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 0,
                                       longitude: 0),
        span: MKCoordinateSpan(latitudeDelta: 180, longitudeDelta: 360))
    
    var body: some View {
        
        
            Map(coordinateRegion: $region)
            .overlay(alignment: .center) {Image("response")}
        
        
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        HeatmapView()
    }
}


